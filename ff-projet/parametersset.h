#ifndef PARAMETERSSET_H
#define PARAMETERSSET_H

#include "setting.h"

//!  Une classe contenant les valeurs actuelles des paramètres d'encodage.
/*!
  Une classe rassemblant tous les paramètres d'encodage et contrôlant leur valeur. Ses membres sont différents "Settings",
  chacun muni de Getters et Setters pour manipuler les différentes valeurs qui seront utilisées par ffmpeg.
*/

class ParametersSet
{
public:
    //! Le constructeur standard de la classe.
    /*!
      Crée une instance de la classe correspondant à l'état "par défaut" des paramètres, que l'on obtient lorsqu'on lance l'application.
      Tous les Settings ont leurs valeurs par défaut.
    */
    ParametersSet();

    //! Getter pour le nom du fichier ouvert.
    /*!
      Retourne la valeur actuelle du nom du fichier ouvert.
    */
    QString getInputName() {return filenameSetting.getInputName();}

    //! Getter pour le nom du fichier d'output.
    /*!
      Retourne la valeur actuelle du nom du fichier d'output.
    */
    QString getOuputName() {return filenameSetting.getOuputName();}

    //! Getter pour le l'offset par rapport au début de la vidéo.
    /*!
      Retourne la valeur actuelle du temps de début de coupure de la vidéo.
    */
    QTime getOffset() {return timeSetting.getOffset();}

    //! Getter pour la durée.
    /*!
      Retourne la valeur actuelle de la durée de la vidéo à encoder.
    */
    QTime getDuration() {return timeSetting.getDuration();}

    //! Getter pour le temps de fin.
    /*!
      Retourne la valeur actuelle du temps de fin de coupure de la vidéo.
    */
    QTime getTimeStop() {return timeSetting.getTimeStop();}

    //! Getter pour le nom du codec audio.
    /*!
      Retourne la valeur actuelle du nom du codec audio.
    */
    QString getAudioCodec() {return audioSetting.getAudioCodec();}

    //! Getter pour la valeur du bitrate audio.
    /*!
      Retourne la valeur actuelle du bitrate audio.
    */
    int getAudioBitrate() {return audioSetting.getAudioBitrate();}

    //! Getter pour le nom du codec vidéo.
    /*!
      Retourne la valeur actuelle du nom du codec vidéo.
    */
    QString getVideoCodec() {return videoSetting.getVideoCodec();}

    //! Getter pour le nom du preset.
    /*!
      Retourne la valeur actuelle du nom du preset.
    */
    QString getPreset() {return videoSetting.getPreset();}

    //! Getter pour la valeur du CRF.
    /*!
      Retourne la valeur actuelle du Constant Rate Factor.
    */
    int getCRF() {return videoSetting.getCRF();}

    //! Getter pour la valeur du bitrate vidéo.
    /*!
      Retourne la valeur actuelle du bitrate vidéo.
    */
    int getVideoBitrate() {return videoSetting.getVideoBitrate();}

    //! Getter pour le suffixe.
    /*!
      Retourne la valeur actuelle du suffixe à ajouter au nom du fichier d'output.
    */
    QString getSuffix() {return suffixSetting.getSuffix();}

    //! Getter pour le nom du réglage.
    /*!
      Retourne la valeur actuelle du nom du réglage englobant tous les paramètres actuels.
    */
    QString getName() {return name;}


    //! Attribue la valeur du nom du fichier ouvert.
    /*!
      \param filename Le nouveau nom de fichier ouvert.
    */
    void setInputName(QString filename) {filenameSetting.setInputName(filename);}

    //! Attribue la valeur du nom du fichier d'output.
    /*!
      \param filename Le nouveau nom de fichier d'output.
    */
    void setOutputName(QString filename) {filenameSetting.setOutputName(filename);}

    //! Attribue la valeur du nouvel offset de temps.
    /*!
      \param time La nouvelle valeur de l'offset.
    */
    void setOffset(QTime time) {timeSetting.setOffset(time);}

    //! Attribue la valeur de la nouvelle durée.
    /*!
      \param time La nouvelle valeur de la durée de la vidéo à encoder.
    */
    void setDuration(QTime time) {timeSetting.setDuration(time);}

    //! Attribue la valeur du temps d'arrêt.
    /*!
      \param time La nouvelle valeur du temps d'arrêt de l'encodage de la vidéo.
    */
    void setTimeStop (QTime time) {timeSetting.setTimeStop(time);}

    //! Attribue la valeur du nom du codec audio.
    /*!
      \param codec Le nouveau nom du codec audio.
    */
    void setAudioCodec(QString codec) {audioSetting.setAudioCodec(codec);}

    //! Attribue la valeur du bitrate audio.
    /*!
      \param bitrate La nouvelle valeur du bitrate audio.
    */
    void setAudioBitrate(int bitrate) {audioSetting.setAudioBitrate(bitrate);}

    //! Attribue la valeur du nom du codec vidéo.
    /*!
      \param codec Le nouveau nom du codec vidéo.
    */
    void setVideoCodec(QString codec) {videoSetting.setVideoCodec(codec);}

    //! Attribue la valeur du preset.
    /*!
      \param new_preset La nouvelle valeur du preset d'encodage.
    */
    void setPreset(QString new_preset) {videoSetting.setPreset(new_preset);}

    //! Attribue la valeur du CRF.
    /*!
      \param new_CRF La nouvelle valeur du Constant Rate Factor.
    */
    void setCRF(int new_CRF) {videoSetting.setCRF(new_CRF);}

    //! Attribue la valeur du bitrate vidéo.
    /*!
      \param bitrate La nouvelle valeur du bitrate vidéo.
    */
    void setVideoBitrate(int bitrate) {videoSetting.setVideoBitrate(bitrate);}

    //! Attribue la valeur du suffixe.
    /*!
      \param new_suffix Le nouveau suffixe à ajouter à la fin du nom d'output.
    */
    void setSuffix(QString new_suffix) {suffixSetting.setSuffix(new_suffix);}

    //! Attribue la valeur du nom du réglage.
    /*!
      \param new_name Le nouveau nom du réglage.
    */
    void setName(QString new_name) {name = new_name;}

protected:
    //! Le nom du réglage.
    QString name;

    //! Le set de paramètres concernant les noms de fichiers.
    FileNameSetting filenameSetting;

    //! Le set de paramètres concernant les temps d'encodage.
    TimeSetting timeSetting;

    //! Le set de paramètres concernant les paramètres audio.
    AudioSetting audioSetting;

    //! Le set de paramètres concernant les paramètres vidéo.
    VideoSetting videoSetting;

    //! Le set de paramètres concernant le suffixe à ajouter à la fin du nom d'output.
    SuffixSetting suffixSetting;
};

#endif // PARAMETERSSET_H
