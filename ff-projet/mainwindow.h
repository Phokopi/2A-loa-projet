#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "ui_mainwindow.h"
#include "parametersset.h"
#include "customfilter.h"
#include "treemodel.h"

#include <QMainWindow>
#include <QModelIndex>
#include <QFile>
#include <QDebug>
#include <QDir>
#include <QFileDialog>
#include <QMessageBox>

#include <QProcess>
#include <QFileInfo>
#include <QShortcut>

namespace Ui {
class MainWindow;
}

//! La classe instanciant la fenêtre principale de l'application.
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    //! Le constructeur standard de la classe.
    explicit MainWindow(QWidget *parent = 0);

    //! Le destructeur standard de la classe.
    ~MainWindow();



private slots:
    //! Slot correspondant au clic sur le bouton "Ouvrir" pour le fichier d'input.
    /*! Récupère les propriétés du fichier et les affiche dans la fenêtre principale.*/
    void on_pushButtonOuvrirInput_clicked();

    //! Slot correspondant au clic sur le bouton "Ouvrir" pour le fichier d'output.
    /*! Récupère le nom du fichier d'output.*/
    void on_pushButtonOuvrirOutput_clicked();

    //! Slot correspondant au clic sur le bouton "Charger".
    /*! Récupère les paramètres du réglage sélectionné dans le modèle, et les applique  dans le diverse champs de l'interface graphique.*/
    void on_pushButtonCharger_clicked();

    //! Slot correspondant au changement de Codec Audio dans l'interface graphique.
    /*! Récupère la nouvelle valeur du codec audio et l'enregistre dans le set de paramètres.*/
    void on_comboBoxCodecAudio_currentIndexChanged(const QString &arg1);

    //! Slot correspondant au changement de Bitrate Audio dans l'interface graphique.
    /*! Récupère la nouvelle valeur du bitrate audio et l'enregistre dans le set de paramètres.*/
    void on_comboBoxBitrateAudio_currentIndexChanged(const QString &arg1);

    //! Slot correspondant au changement de Codec Vidéo dans l'interface graphique.
    /*! Récupère la nouvelle valeur du codec vidéo et l'enregistre dans le set de paramètres.*/
    void on_comboBoxCodecVideo_currentIndexChanged(const QString &arg1);

    //! Slot correspondant au changement de Preset Vidéo dans l'interface graphique.
    /*! Récupère la nouvelle valeur du preset et l'enregistre dans le set de paramètres.*/
    void on_comboBoxPresetVideo_currentIndexChanged(const QString &arg1);

    //! Slot correspondant au clic du bouton "CRF" sur l'interface graphique.
    void on_radioButtonCRF_clicked();

    //! Slot correspondant au clic du bouton "Bitrate Vidéo" sur l'interface graphique.
    void on_radioButtonBitrate_clicked();

    //! Slot correspondant au changement de "Temps de début" sur l'interface graphique.
    /*! Récupère la nouvelle valeur du temps de début et l'enregistre dans le set de paramètres.*/
    void on_timeEditDebut_timeChanged(const QTime &time);

    //! Slot correspondant au changement du Suffixe sur l'interface graphique.
    /*! Récupère la nouvelle valeur du suffixe et l'enregistre dans le set de paramètres.*/
    void on_lineEditSuffixe_textChanged(const QString &arg1);

    //! Slot correspondant au changement du nom du fichier d'input sur l'interface graphique.
    /*! Récupère le nouveau nom et l'enregistre dans le set de paramètres.*/
    void on_lineEditInput_textChanged(const QString &arg1);

    //! Slot correspondant au changement du nom du fichier d'output sur l'interface graphique.
    /*! Récupère le nouveau nom et l'enregistre dans le set de paramètres.*/
    void on_lineEditOutput_textChanged(const QString &arg1);

    //! Slot correspondant au changement du nom du réglage sur l'interface graphique.
    /*! Récupère le nouveau nom et l'enregistre dans le set de paramètres.*/
    void on_lineEditParameterName_textChanged(const QString &arg1);

    //! Slot correspondant au changement de "Durée" sur l'interface graphique.
    /*! Récupère la nouvelle valeur de la duréee et l'enregistre dans le set de paramètres.*/
    void on_timeEditDuree_timeChanged(const QTime &time);

    //! Slot correspondant au clic du bouton "Tempds de fin" sur l'interface graphique.
    void on_radioButtonTempsFin_clicked();

    //! Slot correspondant au clic du bouton "Durée" sur l'interface graphique.
    void on_radioButtonDuree_clicked();

    //! Slot correspondant à un changement de valeur du CRF ou du Bitrate Vidéo.
    /*! Récupère la nouvelle valeur du CRF ou du bitrate et l'enregistre dans le set de paramètres.*/
    void on_spinBoxCRFBitrate_valueChanged(int arg1);

    //! Slot correspondant au clic sur le bouton "Sauvegarder".
    /*! Enregistre les paramètres actuels dans le modèle sous le nom choisi par l'utilisateur.*/
    void on_pushButtonSauvegarder_clicked();

    //! Slot correspondant au clic sur le bouton "Supprimer".
    /*! Supprime le réglage actuellement sélectionné du modèle.*/
    void on_pushButtonSupprimer_clicked();

    //! Slot correspondant au clic sur le bouton "Lancer".
    /*! Lance l'encodage du fichier ouvert avec les paramètres sélctionnés.*/
    void on_pushButtonLancer_clicked();

    //! Slot correspondant à un changement de texte quelconque dans le champ "Filtrage"
    /*! Permet de mettre à jour le filtre du modèle. */
    void on_lineEditFilter_textChanged(const QString &arg1);

private:
    //! Une référence vers l'UI.
    Ui::MainWindow *ui;

    //! Le set de paramètres pour l'encodage de la vidéo.
    /*! Contient toutes les valeurs actuelles des paramètres.*/
    ParametersSet parametersSet;

    //! Change un paramètre dans l'interface graphique.
    /*! Regarde dans le modèle à quel paramètre correspond l'index passé en argument, extrait la valeur de ce paramètre dans le modèle, et la change à la fois dans l'UI et dans le set de paramètres.
     \param index L'index du paramètre à changer. */
    void changeParameter(const QModelIndex & index);

    //! Change les valeurs de tous les paramètres d'encodage dans l'objet JSON passé en argument.
    /*! Extrait les valeurs de chaque paramètre depuis le set de paramètres, et met ces valeurs dans l'objet JSON.
     \param parameterObject L'objet JSON qui va contenir les paramètres du réglage. */
    void fillListParameters(QJsonObject & parameterObject);

    //! Le modèle en arbre contenant les réglages.
    TreeModel *model;

    //! Le modèleproxy servant à filtrer le contenu de la vue.
    customFilter *filterModel;

    //! Le fichier JSON qui contient la liste des régalges.
    QFile fileParametersJson;

    // process ffmpeg
    //! Fonction appellée quand ffmpeg a fini d'encoder.
    /*! Reset la barre de progression.*/
    void on_encoding_finished();

    //! Fonction appellée quand ffmpeg lit le flux d'entrée standard.
    /*! Permet de visualier l'avancement de l'encodage.*/
    void on_process_readyReadStandardOutput();

    //! Fonction appellée quand ffmpeg analyse les informations du fichier en entrée.
    /*! Récupère les propriétés principales de la vidéo à encoder.*/
    void on_info_available();

    // progress bar support
    //! La durée du fichier d'input, en secondes. Sera changé à chaque fois qu'on change le fichier d'input.
    int durationInputFile;

    //! Remplit à une string avec la liste des paramètres d'encodage et la retourne.
    void fillParamList(QStringList & paramList);

    //! Vérifie que le fichier JSON contenant les réglages existe bien.
    /*! \param path Le chemin du fichier JSON.*/
    bool fileExists(QString path);

    //! Convertit un type QTime en un int qui représente le nombre de secondes dans le QTime.
    /*! \param time Le temps sous format QTime.*/
    int qtimeToInt(const QTime & time);

    //! Contient le processus actuellement lancé, pour pouvoir éventuellement le tuer par la suite.
    QProcess* currentProcess;

    //! Vérifie si ffmpeg est disponible dans le PATH.
    bool checkFFMPEG();
};

#endif // MAINWINDOW_H
