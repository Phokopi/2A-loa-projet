#ifndef TREEMODEL_H
#define TREEMODEL_H


#include <QAbstractItemModel>
#include <QModelIndex>
#include <QVariant>

#include "treeitem.h"


class TreeItem;

//!  La classe instanciant le modèle.
/*!
    Le modèle choisi est un modèle en arbre, où en partant d'un élément racine,
    chaque élément a un parent et plusieurs enfants.
*/

class TreeModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    //! Le constructeur standard de la classe.
    /*!
      Instancie le modèle en précisant le parent de l'élément racine.
      \param parent Un élément du modèle qui sera le parent de l'élément racine. Par defaut, ce parent est nul, et l'élément créé sera la racine du modèle.
    */
    explicit TreeModel(QObject *parent = 0);

    //! Le destructeur standard de la classe.
    /*!
      Supprime l'objet racine et par conséquent le reste de ses enfants.
    */
    ~TreeModel();

    //! Crée un modèle à partir du nom d'un fichier JSON.
    /*!
     L'élément racine créé sera l'élément racine du JSON passé en argument.
     \param fileName Le nom du fichier JSON à partir duquel on souhaite créer le modèle.
    */
    bool loadFile(const QString& fileName);

    //! Charge le contenu d'un JSON directement à à partir d'un QJsonDocument.
    /*!
      \param document Le contenu du JSON sous forme d'un QJsonDocument.
    */
    bool loadJson(QJsonDocument & document);

    //! Réécrit le contenu du JSON du modèle, en le remplaçant par le nouveau contenu.
    bool contentJSON();

    //! Retourne la valeur d'un élément du modèle.
    /*!
      Permet d'extraire la valeur d'un élément du modèle si l'on connait son index dans le modèle.
      \param index L'index dans le modèle de l'élément recherché.
      \param role Le rôle de l'élément recherché.
    */
    QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;

    //! Retourne l'index d'un élément du modèle.
    /*!
      Permet d'extraire l'index d'un élément du modèle si l'on connait son emplacement dans le modèle.
      \param row La ligne de l'élément recherché.
      \param column La colonne de l'élément recherché.
      \param parent Le parent de l'élément recherché.
    */
    QModelIndex index(int row, int column,const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;

    //! Retourne l'index du parent d'un élément du modèle.
    /*!
      \param index L'index de l'élément recherché.
    */
    QModelIndex parent(const QModelIndex &index) const Q_DECL_OVERRIDE;

    //! Retourne le nombre de lignes à partir d'un élément du modèle.
    /*!
      \param parent L'index du parent.
    */
    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;

    //! Retourne le nombre de colonnes à partir d'un élément du modèle.
    /*!
      \param parent L'index du parent.
    */
    int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;

private:
    //! Une référence à l'élément racine du modèle.
    TreeItem * mRootItem;

    //! Le document JSON à partir duquel on a créé le modèle.
    QJsonDocument mDocument;

    //! Le contenu des headers du modèle.
    QStringList mHeaders;
};


#endif // TREEMODEL_H
