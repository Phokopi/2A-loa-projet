#ifndef TREEITEM_H
#define TREEITEM_H

#include <QAbstractItemModel>
#include <QJsonDocument>
#include <QJsonValue>
#include <QJsonArray>
#include <QJsonObject>
#include <QIcon>

//!  La classe instanciant les éléments du modèle.
/*!
    Chaque instance de la classe est un élément de l'arbre qui sert de modèle.
    Tous les éléments contiennent un clé, une valeur et des références vers leurs parents et enfants.
*/

class TreeItem
{
public:
    //! Le constructeur standard de la classe.
    /*!
      Instancie un élément du modèle en précisant son parent.
      \param parent Un élément du modèle qui sera le parent de l'élément créé. Par defaut, ce parent est nul, et l'élément créé sera la racine du modèle.
    */
    TreeItem(TreeItem * parent = 0);

    //! Le destructeur standard de la classe.
    /*!
        L'élément ainsi que tous ses enfants sont supprimés.
    */
    ~TreeItem();

    //! Ajoute un élément enfant à l'élément actuel.
    /*!
        Le nouvel élément sera ajouté à la liste des enfants de l'élément actuel.
        \param item L'élément qui deviendra l'enfant de l'élément actuel.
    */
    void appendChild(TreeItem * item);

    //! Permet d'accéder à un des enfants de l'élément actuel.
    /*!
        Retourne un des enfants de l'élément actuel, selon l'indice en argument.
        \param row La ligne de l'enfant par rapport à l'élément actuel.
    */
    TreeItem *child(int row);

    //! Permet d'accéder au parent de l'élément actuel.
    /*!
        Retourne le parent de l'élément actuel.
    */
    TreeItem *parent();

    //! Renvoie le nombre d'enfants de l'élément actuel.
    int childCount() const;

    //! Renvoie la ligne de l'élément actuel.
    int row() const;

    //! Change la clé de l'élément actuel.
    /*!
       \param key La nouvelle clé.
    */
    void setKey(const QString& key);

    //! Change la valeur de l'élément actuel.
    /*!
       \param value La nouvelle valeur.
    */
    void setValue(const QString& value);


    //! Change le type de l'élément actuel.
    /*!
       \param type Le nouveau type.
    */
    void setType(const QJsonValue::Type& type);

    //! Récupère la clé de l'élément actuel.
    QString key() const;

    //! Récupère la valeur de l'élément actuel.
    QString value() const;

    //! Récupère le type de l'élément actuel.
    QJsonValue::Type type() const;

    //! Crée un élément du modèle à partir d'un élément JSON.
    /*!
        Renvoie un élément du modèle construit à partir d'une valeur JSON.
        Cet élément du modèle aura ainsi tous les enfants de l'élément JSON, car la fonction est appellée récursivemet sur chacun des enfants.
        Par défaut, cette fonction est utilisée pour créer l'élément racine de l'arbre.
       \param value L'élément JSON qui servira de base pour l'élément du modèle.
       \param parent Le parent de l'élément qui sera créé. Par defaut, ce parent est nul, et l'élément créé sera la racine du modèle.
    */
    static TreeItem* load(const QJsonValue& value, TreeItem * parent = 0);

protected:


private:
    //! La clé de l'élément.
    QString mKey;

    //! La valeur de l'élément.

    QString mValue;
    //! Le type de l'élément.

    QJsonValue::Type mType;
    //! Une liste de références vers les enfants de l'élément.

    QList<TreeItem*> mChilds;
    //! Une référence vers le parent de l'élément.

    TreeItem * mParent;


};

#endif // TREEITEM_H
