#ifndef CUSTOMFILTER_H
#define CUSTOMFILTER_H

#include <QSortFilterProxyModel>

//!  Une classe permettant de filtrer le contenu de la vue.
/*!
   Cette classe instancie un modèle proxy à partir du modèle original.
   On peut alors librement filtrer le contenu du proxy sans affecter l'original.
   Cette classe hérite de QSortFilterProxyModel, mais on override certaines fonctions pour filtrer de manière plus adaptée à notre situation.
*/

class customFilter : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    //! Le constructeur standard de la classe.
    customFilter(QObject *parent = 0);
protected :
    //! Détermine si une ligne du modèle peut être affichée.
    /*! Cette fonction est overridée afin d'implémenter un filtrage adapté à nos besoins.
        Elle permet ainsi de n'afficher que les réglages correspondant au filtre, mais aussi d'afficher tout leur contenu.
    */
    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override;
};

#endif // CUSTOMFILTER_H
