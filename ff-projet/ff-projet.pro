#-------------------------------------------------
#
# Project created by QtCreator 2018-03-06T16:52:50
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ff-projet
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    treeitem.cpp \
    treemodel.cpp \
    parametersset.cpp \
    customfilter.cpp

HEADERS  += mainwindow.h \
    treeitem.h \
    treemodel.h \
    parametersset.h \
    setting.h \
    customfilter.h
    
FORMS    += mainwindow.ui

CONFIG += c++11
