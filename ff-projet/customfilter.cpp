#include "customfilter.h"
#include <QDebug>
customFilter::customFilter(QObject *parent)
    : QSortFilterProxyModel(parent)
{
}

bool customFilter::filterAcceptsRow(int sourceRow,
        const QModelIndex &sourceParent) const
{
    // On récupère l'index des membres de chaque ligne.
    QModelIndex index0 = sourceModel()->index(sourceRow, 0, sourceParent);

    // On veut toujours afficher les membres de la colonne 1 si leur parent est affiché.
    if (index0.column() == 1) {
        return true;
    }
    // Si l'on est sur la colonne 0
    else {
        // On récupère la ligne du parent de l'objet.
        int row_parent = sourceModel()->parent(index0).row();

        switch (row_parent) {
            // Si on est à la racine
            case -1:
                // On vérifie que le nom du réglage contient le filre.
                return (sourceModel()->data(index0).toString().contains(filterRegExp()));

            // Si on est pas à la racine
            default:
                // On affiche les noms des paramètres si jamais leur parent est affiché.
                return(sourceModel()->parent(index0).data().toString().contains(filterRegExp()));
        }
    }
}

