#include "parametersset.h"

ParametersSet::ParametersSet()
{
    // On initialise tous les Settings avec des paramètres par défaut.
    filenameSetting.setInputName("");
    filenameSetting.setOutputName("");
    timeSetting.setOffset(QTime(0,0));
    timeSetting.setDuration(QTime(0,0));
    timeSetting.setTimeStop(QTime(0,0));
    audioSetting.setAudioBitrate(320);
    audioSetting.setAudioCodec("aac");
    videoSetting.setVideoCodec("libx265");
    videoSetting.setPreset("medium");
    videoSetting.setCRF(23);
    videoSetting.setVideoBitrate(23);

}
