#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent),
      ui(new Ui::MainWindow),
      parametersSet(),
      fileParametersJson((QDir::currentPath()).append("/Liste_reglages.json"))
{
    ui->setupUi(this);

    setWindowTitle(tr("ffmpeg GUI", "Window title"));

    // On empêche le resize de la fenêtre.
    this->setFixedSize(QSize(1143, 660));

    // On initialise le modèle.
    model = new TreeModel();

    // On initialise le modèle proxy pour le filtrage.
    filterModel = new customFilter();
    filterModel->setFilterCaseSensitivity(Qt::CaseInsensitive);

    // Si jamais le fichier JSON contenant les réglages existe déjà, on l'ouvre et on charge le modèle.
    if (fileParametersJson.exists())
    {
        fileParametersJson.open(QIODevice::ReadOnly);
        model->loadFile(fileParametersJson.fileName());
        fileParametersJson.close();
    }
    // Si ce fichier n'existe pas, on le crée.
    else
    {
        fileParametersJson.open(QIODevice::WriteOnly);
        fileParametersJson.close();
    }

    filterModel->setSourceModel(model);

    // On associe le modèle à la vue.
    ui->treeViewParameters->setModel(filterModel);

    // On configure la largeur des colonnes.
    for (int column = 0; column < model->columnCount(); ++column)
    {
        ui->treeViewParameters->setColumnWidth(column, 200);
    }

    // On cache le headers sur la vue.
    ui->treeViewParameters->header()->hide();

    // Vérification de la disponiblité de FFMPEG.
    bool ffmpegAvailable = checkFFMPEG();
    if(ffmpegAvailable) {
        ui->statusBar->showMessage("ffmpeg est bien disponible dans le PATH !");
    } else {
        ui->statusBar->showMessage("ffmpeg n'a pas été trouvé dans le PATH. Assurez-vous de l'ajouter dans votre PATH avant de démarrer ce programme !");
    }

    // Ajout d'un raccourci pour le filtrage.
    QObject::connect(new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_F),  this), &QShortcut::activated, [this](){ ui->lineEditFilter->setFocus(); });
}

MainWindow::~MainWindow()
{
    delete ui;
}

bool MainWindow::checkFFMPEG() {
    int result = system("ffmpeg -version");
    if(result!=0) {
      return false;
    } else {
      return true;
    }

}


// Boutton "Ouvrir ..." de INPUT
void MainWindow::on_pushButtonOuvrirInput_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Save File"),
                               "/home",
                               tr("Video file (*.mp4 *.mkv *.avi *.flv)"));


    // On vérifie que l'utilisateur a bien choisi un fichier à ouvrir.
    if (fileName != "") {

        // On affiche ce nom sur l'UI
        ui->lineEditInput->setText(fileName);

        // On prépare le process ffmpeg utilisé pour récolter les propriétés du fichier.
        QString process = "ffmpeg";
        QStringList paramList;
        paramList << "-i"
                  << fileName;

        // On crée ce proccess et on le connecte au slots adapté pour pouvoir lire son résultat.
        QProcess* FFMPEG = new QProcess();
        FFMPEG->connect(FFMPEG, (void (QProcess::*)(int,QProcess::ExitStatus))&QProcess::finished,
                        this, &MainWindow::on_info_available);

        // On réinitialise les propriétés vidéos, pour éventuellement supprimer celles du fichier précédent.
        ui->labelPropertyAudioCodec->setText("Codec Audio :");
        ui->labelPropertyAudioBitrate->setText("Bitrate Audio :");
        ui->labelPropertyVideoCodec->setText("Codec Vidéo :");
        ui->labelPropertyVideoBitrate->setText("Bitrate Vidéo :");

        // On lance le process.
        FFMPEG->start(process, paramList);
    }
    // Si le nom de fichier est vide, on montre un dialogue d'erreur.
    else {
         QMessageBox messageBox;
         messageBox.critical(0,"Erreur","Nom de fichier vide !");
         messageBox.setFixedSize(500,200);
    }
}

void MainWindow::on_info_available()
{
    // On récupère le process ffmpeg connecté au slot
    QProcess* FFMPEG = static_cast<QProcess *>(QObject::sender());
    QString info = FFMPEG->readAllStandardError();

    // On vérifie que le fichier d'entrée est valide.
    if (info.contains("No such file or directory")) {
        QMessageBox messageBox;
        messageBox.critical(0,"Erreur","Le fichier d'entrée n'existe pas !");
        messageBox.setFixedSize(500,250);
    }
    else
    {
        // On récupère les propriétés audio du fichier.
        if (info.contains("Audio: ", Qt::CaseSensitive)){

            QStringList infoAudio = info.split("Audio: ");

            // On itère sur chacune des pistes audio du fichier.
            for (int i = 1; i < infoAudio.length(); i++) {
                QString audioLine = infoAudio.at(i);

                // Si ce n'est pas la première piste audio, on ajoute un séparateur sur l'UI pour différencier les pistes.
                if (i > 1) {
                    ui->labelPropertyAudioCodec->setText(ui->labelPropertyAudioCodec->text().append(", "));
                    ui->labelPropertyAudioBitrate->setText(ui->labelPropertyAudioBitrate->text().append(", "));
                }
                else {
                    ui->labelPropertyAudioCodec->setText(ui->labelPropertyAudioCodec->text().append(" "));
                    ui->labelPropertyAudioBitrate->setText(ui->labelPropertyAudioBitrate->text().append(" "));
                }

                // On récupère le codec audio de cette piste audio et on l'ajoute à l'UI.
                QString audioCodec = audioLine.split(" ").at(0);
                audioCodec = audioCodec.split(",").at(0);
                ui->labelPropertyAudioCodec->setText(ui->labelPropertyAudioCodec->text().append(audioCodec));

                // Si l'on connait le bitrate pour cette piste audio, on le récupère et on l'ajoute à l'UI.
                if (audioLine.contains(" kb/s", Qt::CaseSensitive)){
                    QString audioBitrate = audioLine.split(" kb/s").at(0);
                    audioBitrate = audioBitrate.split(" ").last();
                    ui->labelPropertyAudioBitrate->setText(ui->labelPropertyAudioBitrate->text().append(audioBitrate).append(" kb/s"));
                }
                // Dans le cas contraire, on ajoute un bitrate inconnu à l'UI.
                else {
                    ui->labelPropertyAudioBitrate->setText(ui->labelPropertyAudioBitrate->text().append("Inconnu"));
                }
            }
        }

        // On récupère le codec vidéo du fichier et on l'ajoute à l'UI.
        if (info.contains("Video: ", Qt::CaseSensitive)){
            QString videoCodec = info.split("Video: ").at(1);
            videoCodec = videoCodec.split(" ").at(0);
            videoCodec = videoCodec.split(",").at(0);
            ui->labelPropertyVideoCodec->setText(ui->labelPropertyVideoCodec->text().append(" ").append(videoCodec));
        }

        // On récupère le bitrate vidéo du fichier et on l'ajoute à l'UI.
        if (info.contains("bitrate: ", Qt::CaseSensitive)){
            QString videoBitrate = info.split("bitrate: ").at(1);
            videoBitrate = videoBitrate.split(" ").at(0);
            ui->labelPropertyVideoBitrate->setText(ui->labelPropertyVideoBitrate->text().append(" ").append(videoBitrate).append(" kb/s"));
        }
    }
}


// Boutton "Ouvrir ..." de OUTPUT
void MainWindow::on_pushButtonOuvrirOutput_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"),
                               "/home",
                               tr("Video file (*.mp4 *.mkv *.avi *.flv)"));


    // On vérifie que l'utilisateur a bien choisi un fichier à ouvrir.
    if (fileName != "") {
         ui->lineEditOutput->setText(fileName);
    }
    // Si le nom de fichier est vide, on montre un dialogue d'erreur.
    else {
         QMessageBox messageBox;
         messageBox.critical(0,"Erreur","Empty filename !");
         messageBox.setFixedSize(500,200);
    }
}

void MainWindow::on_pushButtonCharger_clicked()
{
    const int rootRow = -1;
    const int rootColumn = -1;

    // On récupère l'index actuel dans le vue, ainsi que la ligne et la colonne correspondantes.
    QModelIndex index = ui->treeViewParameters->currentIndex();
    int row = index.row();
    int column = index.column();

    // On vérifie qu'un des éléments de la vie est bien sélectionné.
    if (!(row == rootRow && column == rootColumn)) {
        int columnParent = index.parent().column();

        // On vérifie que ce qui est vérifié est un paramètre d'un réglage et non un réglage.
        if (columnParent == 0) {

            // On change ce paramètre dans l'UI.
            MainWindow::changeParameter(index);

        }
        // Sinon, on a sélectionné un réglage entier.
        // Dans ce cas, on change tous les paramètres dans l'UI conformément à ce réglage.
        else {
            for (int i = 0; i < ui->treeViewParameters->model()->rowCount(index); i++) {
                QModelIndex currentChildIndex = index.child(i, column);
                MainWindow::changeParameter(currentChildIndex);
            }
        }
    }
}

void MainWindow::changeParameter(const QModelIndex & index) {
    int row = index.row();

    // On récupère l'index de la clé et de la valeur à partir de l'index (dans la vue) en argument.
    QModelIndex indexKey = index.sibling(row, 0);
    QModelIndex indexValue = index.sibling(row, 1);

    // On récupère la clé et la valeur à partir de leur index.
    QString parameterName = ui->treeViewParameters->model()->data(indexKey).toString();
    QString parameterValue = ui->treeViewParameters->model()->data(indexValue).toString();

    // Selon le paramètre sélectionné, on effectue le changement adéquat dans l'UI et dans le ParametersSet

    if (parameterName == "Bitrate audio") {
        const int newIndex = ui->comboBoxBitrateAudio->findText(parameterValue);
        ui->comboBoxBitrateAudio->setCurrentIndex(newIndex);
        parameterValue = parameterValue.split(" ").at(0);
        parametersSet.setAudioBitrate(parameterValue.toInt());
    }
    else if (parameterName == "Codec audio") {
        const int newIndex = ui->comboBoxCodecAudio->findText(parameterValue);
        ui->comboBoxCodecAudio->setCurrentIndex(newIndex);
        parametersSet.setAudioCodec(parameterValue);
    }
    else if (parameterName == "Codec video") {
        const int newIndex = ui->comboBoxCodecVideo->findText(parameterValue);
        ui->comboBoxCodecVideo->setCurrentIndex(newIndex);
        parametersSet.setVideoCodec(parameterValue);
    }
    else if (parameterName == "Preset video") {
        const int newIndex = ui->comboBoxPresetVideo->findText(parameterValue);
        ui->comboBoxPresetVideo->setCurrentIndex(newIndex);
        parametersSet.setPreset(parameterValue);
    }
    else if (parameterName == "CRF") {
        // On vérifie si le bouton "CRF" est sélectionné.
        if (! ui->radioButtonCRF->isChecked()) {
            ui->radioButtonCRF->setChecked(true);
            ui->spinBoxCRFBitrate->setMaximum(51);
        }

        int value = parameterValue.toInt();
        ui->spinBoxCRFBitrate->setValue(value);
        parametersSet.setCRF(value);
    }
    else if (parameterName == "Bitrate video") {
        // On vérifie si le bouton "Bitrate vidéo" est sélectionné.
        if (! ui->radioButtonBitrate->isChecked()) {
            ui->radioButtonBitrate->setChecked(true);
            ui->spinBoxCRFBitrate->setMaximum(9999);
        }

        int value = parameterValue.toInt();
        ui->spinBoxCRFBitrate->setValue(value);
        parametersSet.setVideoBitrate(value);
    }
    else if (parameterName == "Suffixe") {
        ui->lineEditSuffixe->setText(parameterValue);
        parametersSet.setSuffix(parameterValue);
    }

}

void MainWindow::on_comboBoxCodecAudio_currentIndexChanged(const QString &arg1)
{
    // Mise à jour du codec audio.
    parametersSet.setAudioCodec(arg1);

    // Si le codec audio est "copy", on désactive le bitrate audio.
    if (arg1 == "copy") {
        ui->comboBoxBitrateAudio->setDisabled(true);
    } else { // Sinon, on le réactive.
        ui->comboBoxBitrateAudio->setDisabled(false);
    }
}


void MainWindow::on_comboBoxBitrateAudio_currentIndexChanged(const QString &arg1)
{
    QString data = arg1;
    data = data.split(" ").at(0);
    parametersSet.setAudioBitrate(data.toInt());
}

void MainWindow::on_comboBoxCodecVideo_currentIndexChanged(const QString &arg1)
{
    parametersSet.setVideoCodec(arg1);
}

void MainWindow::on_comboBoxPresetVideo_currentIndexChanged(const QString &arg1)
{
    parametersSet.setPreset(arg1);
}

void MainWindow::on_radioButtonCRF_clicked()
{
    // On change la valeur avant de changer le maximum, pour que le CRF ne soit pas bloqué à 51 en cas de bitrate vidéo > 51.
    ui->spinBoxCRFBitrate->setValue(parametersSet.getCRF());
    ui->spinBoxCRFBitrate->setMaximum(51);
}

void MainWindow::on_radioButtonBitrate_clicked()
{
    // On change le maximum en premier pour permettre au bitrate vidéo de monter au dessus de 51.
    ui->spinBoxCRFBitrate->setMaximum(9999);
    ui->spinBoxCRFBitrate->setValue(parametersSet.getVideoBitrate());
}

void MainWindow::on_timeEditDebut_timeChanged(const QTime &time)
{
    parametersSet.setOffset(time);
}

void MainWindow::on_lineEditSuffixe_textChanged(const QString &arg1)
{
    parametersSet.setSuffix(arg1);
}

void MainWindow::on_lineEditInput_textChanged(const QString &arg1)
{
    parametersSet.setInputName(arg1);
}

void MainWindow::on_lineEditOutput_textChanged(const QString &arg1)
{
    parametersSet.setOutputName(arg1);
}

void MainWindow::on_lineEditParameterName_textChanged(const QString &arg1)
{
    parametersSet.setName(arg1);
}

void MainWindow::on_timeEditDuree_timeChanged(const QTime &time)
{
    // On crée un temps nul.
    QTime nullTime = QTime(0,0);

    // On caclule la durée en fonction du temps de début et du temps de fin.
    if (ui->radioButtonDuree->isChecked()) {
        parametersSet.setDuration(time);
        int gap_begin_offset = nullTime.secsTo(parametersSet.getOffset());
        int gap_begin_duration = nullTime.secsTo(time);
        int gap = gap_begin_duration + gap_begin_offset;
        parametersSet.setTimeStop(nullTime.addSecs(gap));
    }
    // On calcule le temps de fin à partir du temps de début et de la durée.
    else if (ui->radioButtonTempsFin->isChecked()){
        parametersSet.setTimeStop(time);
        int gap = parametersSet.getOffset().secsTo(time);
        parametersSet.setDuration(nullTime.addSecs(gap));
    }
}

void MainWindow::on_radioButtonTempsFin_clicked()
{
    ui->timeEditDuree->setTime(parametersSet.getTimeStop());
}

void MainWindow::on_radioButtonDuree_clicked()
{
    ui->timeEditDuree->setTime(parametersSet.getDuration());
}

void MainWindow::on_spinBoxCRFBitrate_valueChanged(int arg1)
{
    // On vérifie si le bouton "CRF" est sélectionné.
    if (ui->radioButtonCRF->isChecked()) {
        parametersSet.setCRF(arg1);
    }
    else {
        parametersSet.setVideoBitrate(arg1);
    }
}

void MainWindow::on_pushButtonSauvegarder_clicked()
{
    // On récupère le nom de réglage entré par l'utilisateur et on vérifie qu'il n'est pas vide.
    QString parameterName = parametersSet.getName();

    if (parameterName.isEmpty()){
        QMessageBox messageBox;
        messageBox.critical(0,"Erreur","Le nom du réglage est vide !");
        messageBox.setFixedSize(500,200);
    }
    // Si il n'est pas vide
    else {

        // On ouvre le fichier JSON contenant les réglages.
        fileParametersJson.open(QIODevice::ReadOnly | QIODevice::Text);

        // On récupère un QJsonDocument à partir de ce fichier.
        QJsonParseError JsonParseError;
        QJsonDocument JsonDocument = QJsonDocument::fromJson(fileParametersJson.readAll(), &JsonParseError);

        fileParametersJson.close();

        // On extrait l'objet JSON à partir du QJsonDocument.
        QJsonObject RootObject = JsonDocument.object();

        // On ajoute un suffixe chiffré dans le cas où un réglage portant le même nom est déjà dans le modèle.
        bool name_is_unique = false;
        int increment = 0;
        while(!name_is_unique){
            if (RootObject.contains(parameterName)){
                increment++;
                if (increment != 1) {
                    // Si on a déjà ajouté un suffixe chiffré, on l'enlève en ne considérant pas les 4 dernieres caractères du nom.
                    parameterName = parameterName.left(parameterName.length() - 4);
                }
                parameterName.append(" (").append(QString::number(increment)).append(")");
            }
            else {
                name_is_unique = true;
            }
        }

        // On crée un nouvel objet JSON que l'on remplit des paramètres du ParametersSet.
        QJsonObject newParameterObject = QJsonObject();
        fillListParameters(newParameterObject);

        // On insère le nouveau réglage comment enfant de la racine de l'objet JSON du modèle.
        RootObject.insert(parameterName, newParameterObject);

        // On fait correspondre à notre document JSON le nouvel objet JSON du modèle.
        JsonDocument.setObject(RootObject);

        // On écrit les changements effecutés dans le fichier JSON des réglages.
        fileParametersJson.open(QFile::WriteOnly | QFile::Text | QFile::Truncate);
        fileParametersJson.write(JsonDocument.toJson());
        fileParametersJson.close();

        // On recharge le modèle pour prendre en compte les modifications effectuées.
        this->model->loadJson(JsonDocument);

        // On met à jour la vue pour afficher les changements du modèle.
        ui->treeViewParameters->setModel(filterModel);
        for (int column = 0; column < model->columnCount(); ++column)
        {
            ui->treeViewParameters->setColumnWidth(column, 200);
        }
        ui->treeViewParameters->header()->hide();

        // On réinitialise le filtre.
        ui->lineEditFilter->setText("");
    }
}


void MainWindow::fillListParameters(QJsonObject & parameterObject) {

    // Pour chaque paramètre, on récupère sa valeur dans le ParametersSet et on l'ajoute au JSON avec la bonne clé.

    parameterObject.insert("Bitrate audio", QJsonValue(QString::number(parametersSet.getAudioBitrate()).append(" kbps")));

    // On vérifie si le bouton "CRF" est sélectionné.
    if (ui->radioButtonCRF->isChecked()) {
        parameterObject.insert("CRF", QJsonValue(QString::number(parametersSet.getCRF())));
    } else {
        parameterObject.insert("Bitrate video", QJsonValue(QString::number(parametersSet.getVideoBitrate())));
    }

    parameterObject.insert("Codec audio", QJsonValue(parametersSet.getAudioCodec()));

    parameterObject.insert("Codec video", QJsonValue(parametersSet.getVideoCodec()));

    parameterObject.insert("Preset video", QJsonValue(parametersSet.getPreset()));

    parameterObject.insert("Suffixe", QJsonValue(parametersSet.getSuffix()));
}


void MainWindow::on_pushButtonSupprimer_clicked()
{
    QString parameterName = QString();

    const int rootRow = -1;
    const int rootColumn = -1;

    // On récupère l'index actuel dans le vue, ainsi que la ligne et la colonne correspondantes.
    QModelIndex index = ui->treeViewParameters->currentIndex();
    int row = index.row();
    int column = index.column();

    // On vérifie qu'un des éléments de la vie est bien sélectionné.
    if (!(row == rootRow && column == rootColumn)) {
        int columnParent = index.parent().column();

        // On vérifie que ce qui est vérifié est un paramètre d'un réglage et non un réglage.
        // Dans ce cas, on ne fait rien.
        if (columnParent == 0) {
        }
        // Sinon, on a sélectionné un réglage entier.
        // Dans ce cas, on va le supprimer.
        else {
            // On récupère le nom du réglage sélectionné.
            parameterName = (QString) ui->treeViewParameters->model()->data(index).toString();

            // On ouvre le fichier JSON contenant les réglages.
            fileParametersJson.open(QIODevice::ReadOnly | QIODevice::Text);

            // On récupère un QJsonDocument à partir de ce fichier.
            QJsonParseError JsonParseError;
            QJsonDocument JsonDocument = QJsonDocument::fromJson(fileParametersJson.readAll(), &JsonParseError);

            fileParametersJson.close();

            // On extrait l'objet JSON à partir du QJsonDocument.
            QJsonObject RootObject = JsonDocument.object();

            // On retire le réglage sélectionné du modèle.
            RootObject.remove(parameterName);

            // On fait correspondre à notre document JSON le nouvel objet JSON du modèle.
            JsonDocument.setObject(RootObject);

            // On écrit les changements effecutés dans le fichier JSON des réglages.
            fileParametersJson.open(QFile::WriteOnly | QFile::Text | QFile::Truncate);
            fileParametersJson.write(JsonDocument.toJson());
            fileParametersJson.close();

            // On recharge le modèle pour prendre en compte les modifications effectuées.
            this->model->loadJson(JsonDocument);

            // On met à jour la vue pour afficher les changements du modèle.
            ui->treeViewParameters->setModel(filterModel);
            for (int column = 0; column < model->columnCount(); ++column)
            {
                ui->treeViewParameters->setColumnWidth(column, 200);
            }
            ui->treeViewParameters->header()->hide();
        }
    }
    // On réinitialise le filtre.
    ui->lineEditFilter->setText("");
}

void MainWindow::on_pushButtonLancer_clicked()
{
    // On vérifie que l'encodage n'est pas lancé.
    if (ui->pushButtonLancer->text() == "Lancer") {
        // On récupère le chemin du fichier d'output de l'encodage.
        QString outputPath = parametersSet.getOuputName();

        // On ajoute le suffixe rentré par l'utilisateur.
        // Pour cela, on garde le nom du fichier en enlevant l'extension.
        int lastPosPoint = outputPath.lastIndexOf(".");
        QString outputName = outputPath.left(lastPosPoint);
        QString format = outputPath.right(outputPath.length() - lastPosPoint - 1);

        //On ajoute ensuite le suffixe à ce nom.
        if (! parametersSet.getSuffix().isEmpty()) {
            outputName.append(parametersSet.getSuffix());
        }

        // On vérifie ques les fichiers d'input et d'output ont des noms différents.
        if (parametersSet.getInputName() == outputName.append(".").append(format)) {
            QMessageBox messageBox;
            messageBox.critical(0,"Erreur", "Les fichiers d'input et d'ouput sont les mêmes !");
            messageBox.setFixedSize(500,250);
        }
        else {

            // On prépare le process ffmpeg utilisé pour récolter les propriétés du fichier.
            QString process = "ffmpeg"; // TODO : que ça marche selon l'OS en mettant FFMPEG dans le dossier
            QStringList paramList = QStringList();
            this->fillParamList(paramList);

            // On crée ce proccess et on le connecte au slots adapté pour pouvoir lire son résultat.
            QProcess* FFMPEG = new QProcess();
            FFMPEG->connect(FFMPEG, &QProcess::readyReadStandardError,
                      this, &MainWindow::on_process_readyReadStandardOutput);
            FFMPEG->connect(FFMPEG, (void (QProcess::*)(int,QProcess::ExitStatus))&QProcess::finished,
                      this, &MainWindow::on_encoding_finished);


            // On change le boutont "Lancer" en bouton "Stop".
            ui->pushButtonLancer->setText("Stop");


            // On lance le processus.
            this->currentProcess = FFMPEG;
            FFMPEG->start(process, paramList);
        }
    }
    // Si le processus est en cours, on l'arrête et on remet le bouton "Stop" en bouton "Lancer".
    else {
        ui->pushButtonLancer->setText("Lancer");
        this->currentProcess->kill();
    }


}

void MainWindow::fillParamList(QStringList & paramList) {

    // On récupère le chemin du fichier d'output de l'encodage.
    QString outputPath = parametersSet.getOuputName();

    // On ajoute le suffixe rentré par l'utilisateur.
    // Pour cela, on garde le nom du fichier en enlevant l'extension.
    int lastPosPoint = outputPath.lastIndexOf(".");
    QString outputName = outputPath.left(lastPosPoint);
    QString format = outputPath.right(outputPath.length() - lastPosPoint - 1); // -1 pour enlever le point

    //On ajoute ensuite le suffixe à ce nom.
    if (! parametersSet.getSuffix().isEmpty()) {
        outputName.append(parametersSet.getSuffix());
    }

    // On vérifie que le fichier d'output n'existe pas déjà.
    bool outputFileAlreadyExists = fileExists(outputName + "." + format);

    // Si c'est le cas, on demande à l'utilisateur si il souhaite l'écraser.
    if (outputFileAlreadyExists) {
        QMessageBox::StandardButton reply;
          reply = QMessageBox::question(this, "Problème", "Le fichier de sortie existe déjà. Voulez-vous l'écraser ?",
                                        QMessageBox::Yes|QMessageBox::No);
          if (reply == QMessageBox::Yes) {
            paramList << "-y";
          } else {
            paramList << "-n";
          }
    }

    // On récupère depuis le ParametersSet la liste des paramètres pour l'encodage vidéo.
    paramList << "-i" << parametersSet.getInputName()
              << "-c:v" << parametersSet.getVideoCodec()
              << "-c:a" << parametersSet.getAudioCodec();

    // Si le bitrate audio est activé (i.e. le codec audio n'est pas "copy").
    if (ui->comboBoxBitrateAudio->isEnabled()) {
        paramList << "-b:a" << QString::number(parametersSet.getAudioBitrate()) + "k";
    }


    paramList << "-preset" << parametersSet.getPreset();


    // Selon que l'utilisateur ait choisi "CRF" ou "Bitrate vidéo", un des deux paramètres est utilisé.
    if (ui->radioButtonCRF->isChecked()) {
        paramList << "-crf" << QString::number(parametersSet.getCRF());
    }
    else {
        paramList << "-b:v" << QString::number(parametersSet.getVideoBitrate()) + "k";
    }

    // Si le fichier est un ".mkv", on ajoute l'option "-map" pour garder tous les flux (audio, vidéo, sous-titres, etc...)
    if (format == "mkv") {
        paramList << "-map" << "0";
    }


    // On récupère le temps de début d'encoddage de la vidéo.
    QString timeStart = parametersSet.getOffset().toString("HH:mm:ss.zzz");

    // Si ce temps est le tout début de la vidéo, on peut ajouter l'option "-ss" pour simplifier.
    if (timeStart != "00:00:00.000") {
        paramList << "-ss" << timeStart;
    }

    // Si l'utilisateur a choisi d'utiliser la durée d'encodage.
    if (ui->radioButtonDuree->isChecked()) {
        QString duree = parametersSet.getDuration().toString("HH:mm:ss.zzz");
        paramList << "-t" << duree;
    }
    // Si l'utilisateur a choisi d'utiliser le temps de fin d'encodage.
    else if (ui->radioButtonTempsFin->isChecked()) {
        QString tempsFin = parametersSet.getTimeStop().toString("HH:mm:ss.zzz");
        paramList << "-to" << tempsFin;
    }
    // Si l'utilisateur a chosi d'aller jusqu'au bout, il n'y a rien à ajouter dans la ligne de commande, car c'est l'option par défaut pour ffmpeg.


    // On ajoute des options pour être sûrs de pouvoir encoder l'audio dans tous les cas.
    paramList << "-strict" << "-2"
              << outputName + "." + format;

}


void MainWindow::on_encoding_finished()
{
    // On met la barre de progression à fond.
    ui->progressBar->setValue(100);

    // On change le bouton "Stop" en bouton "Lancer".
    ui->pushButtonLancer->setText("Lancer");
}

void MainWindow::on_process_readyReadStandardOutput()
{
    // On récupère le process ffmpeg connecté au slot
    QProcess* FFMPEG = static_cast<QProcess *>(QObject::sender());
    QString line = FFMPEG->readAllStandardError();

    // On vérifie que le format fichier de sortie est valide.
    if (line.contains("Invalid argument")){
        QMessageBox messageBox;
        messageBox.critical(0,"Erreur","Le format du fichier de sortie n'est pas correct !");
        messageBox.setFixedSize(500,250);
    }
    // On vérifie que le fichier de sortie existe.
    else if (line.contains("No such file or directory")) {
        QMessageBox messageBox;
        messageBox.critical(0,"Erreur","Le fichier d'entrée n'existe pas !");
        messageBox.setFixedSize(500,250);
    }
    // Si on lit des informations sur la vidéo.
    else if (line.contains("Duration: ", Qt::CaseSensitive)) {

        // On récupère des informations sur sa durée.

        QString timeAndStuff = line.split("Duration: ").at(1);
        QString timeString = timeAndStuff.split(",").at(0);
        QString timeWithoutMs = timeString.split(".").at(0);

        QTime time = QTime::fromString(timeWithoutMs, "hh:mm:ss");

        // Si le durée est valide
        if(time.isValid()) {
            // seconds ellapsed
            const int seconds = QTime(0,0).secsTo(time);

            // update percentage done in progress bar
            this->durationInputFile = seconds;
        }
    }
    // if encoding
    else if (line.startsWith("frame")) {

        // retrieve current time

        QString timeAndStuff = line.split("time=").at(1);
        QString timeString = timeAndStuff.split(",").at(0);
        QString timeWithoutMs = timeString.split(".").at(0);

        QTime time = QTime::fromString(timeWithoutMs, "hh:mm:ss");

        if(time.isValid()) {
            // seconds ellapsed
            const int seconds = QTime(0,0).secsTo(time);

            // get duration of the video, depending on cases
            int secondsFromBeginning = qtimeToInt(parametersSet.getOffset());
            int videoDurationInSeconds;

            if (ui->radioButtonTempsFin->isChecked()) {
                videoDurationInSeconds = qtimeToInt(parametersSet.getTimeStop()) - secondsFromBeginning;
            }
            else if (ui->radioButtonDuree->isChecked()) {
                videoDurationInSeconds = qtimeToInt(parametersSet.getDuration());
            }
            else { // to the end
                videoDurationInSeconds = this->durationInputFile;
            }

            // update percentage done in progress bar
            const float percentageFinished = 100 * (float)seconds / (float)videoDurationInSeconds;
            ui->progressBar->setValue((int)percentageFinished);
        }
    }
}

int MainWindow::qtimeToInt(const QTime & time) {
    return time.hour() * 3600 + time.minute() * 60 + time.second();
}

bool MainWindow::fileExists(QString path) {
    QFileInfo check_file(path);
    // check if file exists and if yes: Is it really a file and no directory?
    return check_file.exists() && check_file.isFile();
}

void MainWindow::on_lineEditFilter_textChanged(const QString &arg1)
{
    filterModel->setFilterRegExp(arg1);
}
