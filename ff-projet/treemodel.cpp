#include <QtWidgets>

#include "treemodel.h"

TreeModel::TreeModel(QObject *parent) :
    QAbstractItemModel(parent)
{
    mRootItem = new TreeItem;
}

TreeModel::~TreeModel()
{
    delete mRootItem;
}

bool TreeModel::loadFile(const QString &fileName)
{
    // On crée le document JSON en lisant le contenu du fichier en argument.
    QFile file(fileName);
    bool success = false;
    if (file.open(QIODevice::ReadOnly)) {
        mDocument = QJsonDocument::fromJson(file.readAll());
        // On réécrit le contenu de mDocument avec les nouvelles données.
        success = contentJSON();
        file.close();
    }
    else success = false;

    return success;
}

bool TreeModel::loadJson(QJsonDocument & document)
{
    mDocument = document;
    return contentJSON();
}

bool TreeModel::contentJSON() {

    // On vérifie que le document est valide
    if (!mDocument.isNull())
    {
        // On reset le modèle, en chargeant récursivement chaque élément, en commençant par la racine.
        beginResetModel();
        delete mRootItem;
        mRootItem = TreeItem::load(QJsonValue(mDocument.object()));
        endResetModel();
        return true;
    }

    qDebug()<<Q_FUNC_INFO<<"Impossible de charger le JSON";
    return false;
}

QVariant TreeModel::data(const QModelIndex &index, int role) const
{
    // On vérifie que l'index en argument est valide. Dans le cas contraire, on retourne l'index racine.
    if (!index.isValid())
        return QVariant();

    // On récupère le pointeur interne de l'élément à l'index ciblé.
    TreeItem *item = static_cast<TreeItem*>(index.internalPointer());


    // On vérifie si il s'agit d'une clé ou d'une valeur, et on en extrait la bonne donnée, que l'on retourne.
    if (role == Qt::DisplayRole) {
        if (index.column() == 0)
            return QString("%1").arg(item->key());

        if (index.column() == 1)
            return QString("%1").arg(item->value());
    }

    // En cas d'échec, on retourne un QVariant vide.
    return QVariant();

}

QModelIndex TreeModel::index(int row, int column, const QModelIndex &parent) const
{
    // On vérifie que l'index du parent est valide. Dans le cas contraire, on retourne l'index racine.
    if (!hasIndex(row, column, parent))
        return QModelIndex();

    // On vérifie ensuite que le parent en lui-même est valide.
    // Si il ne l'est pas, on renvoie l'élément racine.
    // Si il l'est, on accède à son pointeur interne.
    TreeItem *parentItem;
    if (!parent.isValid())
        parentItem = mRootItem;
    else
        parentItem = static_cast<TreeItem*>(parent.internalPointer());

    // On peut alors extraire l'index de l'enfant souhaité, grâce à la ligne et à la colonne en argument.
    TreeItem *childItem = parentItem->child(row);
    if (childItem)
        return createIndex(row, column, childItem);
    // Si aucun enfant valide n'est trouvé, on retourne l'index racine.
    else
        return QModelIndex();
}

QModelIndex TreeModel::parent(const QModelIndex &index) const
{
    // On vérifie que l'index en argument est valide. Dans le cas contraire, on retourne l'index racine.
    if (!index.isValid())
        return QModelIndex();

    // On accède au pointeur interne de l'élément ciblé et on en extrait l'élément parent.
    TreeItem *childItem = static_cast<TreeItem*>(index.internalPointer());
    TreeItem *parentItem = childItem->parent();

    // Si le parent est la racine, on retourne l'index racine.
    if (parentItem == mRootItem)
        return QModelIndex();

    // Sinon, on crée l'index correspondant au parent.
    return createIndex(parentItem->row(), 0, parentItem);
}

int TreeModel::rowCount(const QModelIndex &parent) const
{
    TreeItem *parentItem;
    if (parent.column() > 0)
        return 0;

    // Si le parent n'est pas valide, on se place à la racine.
    if (!parent.isValid())
        parentItem = mRootItem;
    // Sinon, on récupère le pointeur interne du parent.
    else
        parentItem = static_cast<TreeItem*>(parent.internalPointer());

    return parentItem->childCount();
}

int TreeModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return 2;
}
