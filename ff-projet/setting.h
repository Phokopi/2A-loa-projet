#ifndef SETTING_H
#define SETTING_H

#include <QString>
#include <QTime>

class Setting {};

class TemporarySetting : public Setting {};

//!  Le set de paramètres concernant les noms de fichiers.
class FileNameSetting : public TemporarySetting
{
protected:
    //! Le nom du fichier ouvert.
    QString inputName;

    //! Le nom du fichier d'output.
    QString outputName;

public:
   //! Getter pour le nom du fichier ouvert.
   /*!
     Retourne la valeur actuelle du nom du fichier ouvert.
   */
   QString getInputName() {return inputName;}

   //! Getter pour le nom du fichier d'output.
   /*!
     Retourne la valeur actuelle du nom du fichier d'output.
   */
   QString getOuputName() {return outputName;}

   //! Attribue la valeur du nom du fichier ouvert.
   /*!
     \param filename Le nouveau nom de fichier ouvert.
   */
   void setInputName(QString filename) {inputName = filename;}

   //! Attribue la valeur du nom du fichier d'output.
   /*!
     \param filename Le nouveau nom de fichier d'output.
   */
   void setOutputName(QString filename) {outputName = filename;}
};

//! Le set de paramètres concernant les temps d'encodage.
class TimeSetting : public TemporarySetting
{
protected:
    //! Le temps de début de coupure de la vidéo.
    QTime offsetFromBegenning;

    //! La durée de la vidéo à encoder.
    QTime duration;

    //! Le temps de fin de coupure de la vidéo.
    QTime timeToStop;

public:
   //! Getter pour le l'offset par rapport au début de la vidéo.
   /*!
     Retourne la valeur actuelle du temps de début de coupure de la vidéo.
   */
   QTime getOffset() {return offsetFromBegenning;}

   //! Getter pour la durée.
   /*!
     Retourne la valeur actuelle de la durée de la vidéo à encoder.
   */
   QTime getDuration() {return duration;}

   //! Getter pour le temps de fin.
   /*!
     Retourne la valeur actuelle du temps de fin de coupure de la vidéo.
   */
   QTime getTimeStop() {return timeToStop;}

   //! Attribue la valeur du nouvel offset de temps.
   /*!
     \param time La nouvelle valeur de l'offset.
   */
   void setOffset(QTime time) {offsetFromBegenning = time;}

   //! Attribue la valeur de la nouvelle durée.
   /*!
     \param time La nouvelle valeur de la durée de la vidéo à encoder.
   */
   void setDuration(QTime time) {duration = time;}

   //! Attribue la valeur du temps d'arrêt.
   /*!
     \param time La nouvelle valeur du temps d'arrêt de l'encodage de la vidéo.
   */
   void setTimeStop (QTime time) {timeToStop = time;}
};


class PermanentSetting : public Setting {};

//! Le set de paramètres concernant les réglages audio.
class AudioSetting : public PermanentSetting
{
protected:
    //! Le nom du codec audio.
    QString audioCodec;

    //! La valeur du bitrate audio en kilobit par secondes.
    int audioBitrate;

public:
   //! Getter pour le nom du codec audio.
   /*!
     Retourne la valeur actuelle du nom du codec audio.
   */
   QString getAudioCodec() {return audioCodec;}

   //! Getter pour la valeur du bitrate audio.
   /*!
     Retourne la valeur actuelle du bitrate audio.
   */
   int getAudioBitrate() {return audioBitrate;}

   //! Setter pour la valeur du codec audio.
   /*! \param codec Le nouveau codec.*/
   void setAudioCodec(QString codec) {audioCodec = codec;}

   //! Setter pour la valeur du bitrate audio.
   /*! \param bitrate Le nouveau bitrate.*/
   void setAudioBitrate(int bitrate) {audioBitrate = bitrate;}
};

//! Le set de paramètres concernant les réglages vidéo.
class VideoSetting : public PermanentSetting
{
protected:
    //! Le nom du codec vidéo.
    QString videoCodec;

    //! Le preset de compression.
    QString preset;

    //! La valeur du Constant Rate Factor
    int CRF;

    //! La valeur du bitrate vidéo.
    int videoBitrate;

public:
   //! Getter pour le nom du codec vidéo.
   /*!
     Retourne la valeur actuelle du nom du codec vidéo.
   */
   QString getVideoCodec() {return videoCodec;}

   //! Getter pour le nom du preset.
   /*!
     Retourne la valeur actuelle du nom du preset.
   */
   QString getPreset() {return preset;}

   //! Getter pour la valeur du CRF.
   /*!
     Retourne la valeur actuelle du Constant Rate Factor.
   */
   int getCRF() {return CRF;}

   //! Getter pour la valeur du bitrate vidéo.
   /*!
     Retourne la valeur actuelle du bitrate vidéo.
   */
   int getVideoBitrate() {return videoBitrate;}

   //! Attribue la valeur du nom du codec vidéo.
   /*!
     \param codec Le nouveau nom du codec vidéo.
   */
   void setVideoCodec(QString codec) {videoCodec = codec;}

   //! Attribue la valeur du preset.
   /*!
     \param new_preset La nouvelle valeur du preset d'encodage.
   */
   void setPreset(QString new_preset) {preset = new_preset;}

   //! Attribue la valeur du CRF.
   /*!
     \param new_CRF La nouvelle valeur du Constant Rate Factor.
   */
   void setCRF(int new_CRF) {CRF = new_CRF;}

   //! Attribue la valeur du bitrate vidéo.
   /*!
     \param bitrate La nouvelle valeur du bitrate vidéo.
   */
   void setVideoBitrate(int bitrate) {videoBitrate = bitrate;}
};

//! Le set de paramètres concernant le suffixe.
class SuffixSetting : public PermanentSetting
{
protected:
    //! Le suffixe à ajouter à la fin du nom d'output.
    QString outputNameSuffix;

public:
   //! Getter pour le suffixe.
   /*!
     Retourne la valeur actuelle du suffixe à ajouter au nom du fichier d'output.
   */
   QString getSuffix() {return outputNameSuffix;}

   //! Attribue la valeur du suffixe.
   /*!
     \param new_suffix Le nouveau suffixe à ajouter à la fin du nom d'output.
   */
   void setSuffix(QString new_suffix) {outputNameSuffix = new_suffix;}
};


#endif // SETTING_H
