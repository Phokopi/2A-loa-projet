#include "treeitem.h"

#include <QFile>
#include <QDebug>
#include <QFont>


TreeItem::TreeItem(TreeItem *parent)
{
    mParent = parent;
}

TreeItem::~TreeItem()
{
    qDeleteAll(mChilds);
}

void TreeItem::appendChild(TreeItem *item)
{
    mChilds.append(item);
}

TreeItem *TreeItem::child(int row)
{
    return mChilds.value(row);
}

TreeItem *TreeItem::parent()
{
    return mParent;
}

int TreeItem::childCount() const
{
    return mChilds.count();
}

int TreeItem::row() const
{
    if (mParent)
        return mParent->mChilds.indexOf(const_cast<TreeItem*>(this));

    return 0;
}

void TreeItem::setKey(const QString &key)
{
    mKey = key;
}

void TreeItem::setValue(const QString &value)
{
    mValue = value;
}

void TreeItem::setType(const QJsonValue::Type &type)
{
    mType = type;
}

QString TreeItem::key() const
{
    return mKey;
}

QString TreeItem::value() const
{
    return mValue;
}

QJsonValue::Type TreeItem::type() const
{
    return mType;
}

TreeItem* TreeItem::load(const QJsonValue& value, TreeItem* parent)
{

    // On définit le nouvel élément comme une racine par rapport à son parent passé en argument.
    TreeItem * rootItem = new TreeItem(parent);
    rootItem->setKey("root");

    // Si la valeur de cet élément est un objet JSON
    if ( value.isObject())
    {
        // On rappelle la fonction sur chacun des enfants pour compléter le modèle.
        for (QString key : value.toObject().keys()){
            QJsonValue v = value.toObject().value(key);
            TreeItem * child = load(v,rootItem);
            child->setKey(key);
            child->setType(v.type());
            rootItem->appendChild(child);
        }
    }
    // Si la valeur de cet élément est un tableau
    else if ( value.isArray())
    {
        // On rappelle la fonction sur chacun des éléments du tableau pour compléter le modèle.
        int index = 0;
        for (QJsonValue v : value.toArray()){

            TreeItem * child = load(v,rootItem);
            child->setKey(QString::number(index));
            child->setType(v.type());
            rootItem->appendChild(child);
            ++index;
        }
    }
    // Sinon, on attribue à l'élément sa valeur.
    else
    {
        rootItem->setValue(value.toVariant().toString());
        rootItem->setType(value.type());
    }

    return rootItem;
}
